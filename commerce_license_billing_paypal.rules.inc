<?php
/**
 * @file
 * Rules integration for the Commerce License Billing Paypal module.
 */

/**
* Implementation of hook_rules_event_info().
*/
function commerce_license_billing_paypal_rules_event_info() {
  return array(
    'commerce_license_billing_paypal_error' => array(
      'label' => t('After receiving Paypal error'),
      'module' => 'commerce_license_billing_paypal',
      'group' => t('Commerce License Billing Paypal'),
      'variables' => array(
        'order' => array('type' => 'commerce_order', 'label' => t('The order that failed.')),
      ),
    ),
  );
}

/**
 * Implements hook_rules_condition_info().
 */
function commerce_license_billing_paypal_rules_condition_info() {
  $conditions['commerce_license_billing_paypal_recurring_condition'] = array(
    'label' => t('Order is to be paid for with Paypal recurring payment.'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'group' => t('Commerce License Billing Paypal'),
    'callbacks' => array(
      'execute' => 'commerce_license_billing_paypal_recurring_condition',
    ),
  );
  return $conditions;
}

/**
 * Rules condition callback: check if an order is a paypal recurring payment.
 */
function commerce_license_billing_paypal_recurring_condition($order) {

  $isPaypal = false;
  $payment_method = '';

  if (isset($order->data['payment_method'])) {
    $payment_method = $order->data['payment_method'];
  }
  elseif (isset($order->data['first_payment_method'])) {
    $payment_method = $order->data['first_payment_method'];
  }

  $payment_method_id = explode('|', $payment_method)[0];

  if ('paypal_ec_subscription' == $payment_method_id) {
    $isPaypal = true;
  }

  return $isPaypal;
}

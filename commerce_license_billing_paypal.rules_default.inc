<?php

/**
 * @file
 * Rules integration for the Commerce License Billing Paypal module.
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_license_billing_paypal_default_rules_configuration() {
  $items['rules_commerce_license_billing_paypal_verify_recurring_payment'] = entity_import('rules_config', '{ "rules_commerce_license_billing_paypal_verify_recurring_payment" : {
    "LABEL" : "Verify a Paypal recurring order",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "TAGS" : [ "Commerce License Billing Paypal" ],
    "REQUIRES" : [
      "rules",
      "commerce_payment",
      "entity"
    ],
    "ON" : { "commerce_order_update" : [] },
    "IF" : [
      { "NOT data_is" : {
          "data" : [ "commerce-order:status" ],
          "value" : [ "commerce-order-unchanged:status" ]
        }
      },
      { "data_is" : {
          "data" : [ "commerce-order:status" ],
          "value" : "recurring_payment_pending"
        }
      },
      { "commerce_payment_order_balance_comparison" : {
          "commerce_order" : [ "commerce_order" ],
          "operator" : "\u003E",
          "value" : "0"
        }
      },
      { "commerce_license_billing_paypal_recurring_condition" : { "commerce_order" : [ "commerce_order" ] } }
    ],
    "DO" : [
      { "commerce_lcardonfile_order_select_card" : {
          "USING" : { "order" : [ "commerce-order" ] },
          "PROVIDE" : { "select_card_response" : { "select_card_response" : "Select card response" } }
        }
      },
      { "commerce_cardonfile_order_charge_card" : {
          "USING" : {
            "order" : [ "commerce-order" ],
            "charge" : [ "" ],
            "select_card_response" : [ "select_card_response" ],
            "card_data" : [ "" ]
          },
          "PROVIDE" : { "charge_card_response" : { "charge_card_response" : "charge Card Response" } }
        }
      }
    ]
  }
}');

  //return $items;
}
